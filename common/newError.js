"use strict";

function newError(code, message, severity, layer) {
  return {
    code,
    message,
    severity,
    layer,
  };
}

module.exports = { newError };
