"use strict";

const { capitalize } = require("../common/capitalizeString");

function capitalizeObjParams(obj = {}) {
  Object.keys(obj).forEach(function (key) {
    obj[key] = capitalize(obj[key]);
  });
}

module.exports = { capitalizeObjParams };
