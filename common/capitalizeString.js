"use strict";

function capitalize(str = "") {
  if (str) {
    let resultStr;
    resultStr = str.trim().replace(/ +(?= )/g, "");
    return resultStr[0].toUpperCase() + resultStr.slice(1).toLowerCase();
  }
}

module.exports = { capitalize };
