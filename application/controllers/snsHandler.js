const { publishNotification } = require("../../infrastructure/snsService");

async function snsHandler() {
  try {
    return await publishNotification(
      "DynamoDB Updated",
      "alerta",
      process.env.TOPIC_ARN
    );
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify({
        code: "internal_server_error",
        severity: "HIGH",
        message: "Internal Server Error",
        details: err.toString(),
      }),
    };
  }
}

module.exports = { snsHandler };
