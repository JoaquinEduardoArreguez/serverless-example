"use strict";

const { capitalizeObjParams } = require("../../common/capitalizeObjParams");
const { bffPostCar } = require("../../domain/bffPostCar");
const { carsSchema, idSchema } = require("../schemas/dbReqSchema");
const { newError } = require("../../common/newError");

async function bffHandler(req, res) {
  let carId, carData;
  try {
    capitalizeObjParams(req.body);
    carId = await idSchema.validateAsync(req.body.id);
    delete req.body.id;
    carData = await carsSchema.validateAsync(req.body);
  } catch (error) {
    return res.status(400).json(error);
  }

  try {
    const result = await bffPostCar(carData, carId);
    return res.status(200).json(JSON.parse(result.text));
  } catch (error) {
    return res
      .status(500)
      .json(
        newError(
          "Internal Server Error",
          "internal server error",
          "HIGH",
          "infrastructure"
        )
      );
  }
}

module.exports = { bffHandler };
