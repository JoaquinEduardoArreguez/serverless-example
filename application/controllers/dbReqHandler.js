"use strict";
const uuid = require("uuid-random");
const tableName = process.env.CAR_TABLE_NAME;

const { put, get, remove } = require("../../infrastructure/dynamoDBService");
const { carsSchema, idSchema } = require("../schemas/dbReqSchema");
const { newError } = require("../../common/newError");

async function getCar(req, res) {
  try {
    await idSchema.validateAsync(req.params.id);
  } catch (error) {
    return res.status(400).json(error);
  }
  try {
    const result = await get(tableName, req.params.id);
    return res.status(200).json(result.Item);
  } catch (error) {
    return res
      .status(500)
      .json(
        newError(
          "Internal Server Error",
          "internal server error",
          "HIGH",
          "infrastructure"
        )
      );
  }
}

async function putCar(req, res) {
  const id = req.params.id || uuid();

  try {
    await idSchema.validateAsync(id);
    await carsSchema.validateAsync(req.body);
    req.body.id = id;
  } catch (error) {
    return res.status(400).json(error);
  }

  try {
    await put(tableName, req.body);
    return res.status(200).json(req.body);
  } catch (error) {
    return res
      .status(500)
      .json(
        newError(
          "Internal Server Error",
          "internal server error",
          "HIGH",
          "infrastructure"
        )
      );
  }
}

async function removeCar(req, res) {
  try {
    await idSchema.validateAsync(req.params.id);
  } catch (error) {
    return res.status(400).json(error);
  }

  try {
    await remove(tableName, req.params.id);
    return res.status(200).json({ id: req.params.id });
  } catch (error) {
    return res
      .status(500)
      .json(
        newError(
          "Internal Server Error",
          "internal server error",
          "HIGH",
          "infrastructure"
        )
      );
  }
}

module.exports = { putCar, getCar, removeCar };
