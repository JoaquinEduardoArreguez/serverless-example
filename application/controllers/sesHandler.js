"use strict";

const { sendTemplatedEmail } = require("../../domain/sesMethods");
const { getEmailBody } = require("../../domain/getEmailBody");

const from = "Yamil Karky <ykarqui@gmail.com>";
const to = ["arreguez.joaquin@gmail.com"];
let htmlMsgBody = "";
const subject = "SES IS WORKING";

async function sesHandler() {
  try {
    htmlMsgBody = await getEmailBody("emailTemplate.html");
    return await sendTemplatedEmail(from, to, subject, htmlMsgBody);
  } catch (err) {
    return {
      statusCode: 500,
      body: JSON.stringify({
        code: "internal_server_error",
        severity: "HIGH",
        message: "Internal Server Error",
        details: err.toString(),
      }),
    };
  }
}

module.exports = { sesHandler };
