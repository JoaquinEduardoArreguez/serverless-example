"use strict";
const serverless = require("serverless-http");
const express = require("express");
const app = express();
const bodyParser = require("body-parser");

const { getCar, putCar, removeCar } = require("../controllers/dbReqHandler");
const { bffHandler } = require("../controllers/bffHandler");
app.use(bodyParser.json({ strict: false }));

// DynamoDB endpoints
app.post("/db/cars", putCar);
app.get("/db/cars/:id", getCar);
app.put("/db/cars/:id", putCar);
app.delete("/db/cars/:id", removeCar);

// BFF endpoints
app.post("/bff/cars", bffHandler);

module.exports.routes = serverless(app);
