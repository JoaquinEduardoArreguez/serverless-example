const joi = require("@hapi/joi");

const carsSchema = joi
  .object({
    brand: joi
      .string()
      .regex(/^[a-zA-Z0-9 ]*$/)
      .min(1)
      .max(20)
      .required(),
    model: joi
      .string()
      .regex(/^[a-zA-Z0-9 ]*$/)
      .min(1)
      .max(20)
      .required(),
    car_img: joi.string().uri(),
  })
  .error((errors) => {
    validationError(
      "Bad Request",
      errors[0].toString(),
      "MEDIUM",
      "application"
    );
  });

const idSchema = joi
  .alternatives()
  .try(joi.string().guid(), joi.number())
  .required()
  .error(() => {
    validationError(
      "Bad Request",
      "id must be a valid number or uuid",
      "MEDIUM",
      "application"
    );
  });

function validationError(code, message, severity, layer) {
  const error = new Error();
  Object.assign(error, {
    code,
    message,
    severity,
    layer,
  });
  throw error;
}

module.exports = { carsSchema, idSchema };
