"use strict";
const AWS = require("aws-sdk");

const dynamoDb = new AWS.DynamoDB.DocumentClient();

async function get(tableName, id) {
  const params = {
    TableName: tableName,
    Key: {
      id: id,
    },
  };

  return dynamoDb.get(params).promise();
}

async function put(tableName, body) {
  const params = {
    TableName: tableName,
    Item: body,
  };

  return dynamoDb.put(params).promise();
}

async function remove(tableName, id) {
  const params = {
    TableName: tableName,
    Key: {
      id: id,
    },
  };

  return dynamoDb.delete(params).promise();
}

module.exports = { put, get, remove };
