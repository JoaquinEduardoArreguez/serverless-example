"use strict";

const AWS = require("aws-sdk");
const ses = new AWS.SES({ region: "us-east-1" });

async function createEmailTemplate(
  templateName,
  emailSubject,
  htmlMsgBody,
  textMsg = ""
) {
  const templateParams = {
    Template: {
      TemplateName: templateName,
      SubjectPart: emailSubject,
      TextPart: textMsg,
      HtmlPart: htmlMsgBody,
    },
  };
  return ses.createTemplate(templateParams).promise();
}

async function sendEMail(
  templateName,
  fromEmailAddress,
  toEmailAddresses,
  templateData = "{}"
) {
  const sendParams = {
    Source: fromEmailAddress,
    Template: templateName,
    Destination: {
      ToAddresses: toEmailAddresses,
    },
    TemplateData: templateData,
  };
  return ses.sendTemplatedEmail(sendParams).promise();
}

async function deleteEmailTemplate(templateName) {
  return ses.deleteTemplate({ TemplateName: templateName }).promise();
}

module.exports = { createEmailTemplate, deleteEmailTemplate, sendEMail };
