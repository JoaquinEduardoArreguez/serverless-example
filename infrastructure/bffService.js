"use strict";
const superagent = require("superagent");

const REQ_PATH = process.env.DYNAMODB_ENDPOINT + "/db/cars/";

async function getCar(id) {
  return superagent.get(REQ_PATH + id);
}

async function putCar(body, id) {
  return superagent.put(REQ_PATH + id).send(body);
}

async function postCar(body) {
  return superagent.post(REQ_PATH).send(body);
}

module.exports = { getCar, putCar, postCar };
