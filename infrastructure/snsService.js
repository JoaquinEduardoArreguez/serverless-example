const AWS = require("aws-sdk");

const sns = new AWS.SNS({
  region: "us-east-1",
});

async function publishNotification(message, subject, topicArn) {
  var params = {
    Message: message,
    Subject: subject,
    TopicArn: topicArn,
  };
  return sns.publish(params).promise();
}

module.exports = { publishNotification };
