"use strict";

const AWS = require("aws-sdk");
const s3 = new AWS.S3();

async function getFromS3(bucketName, fileName) {
  let params = {
    Bucket: bucketName,
    Key: fileName,
  };
  const data = await s3.getObject(params).promise();
  return data.Body.toString();
}

module.exports = { getFromS3 };
