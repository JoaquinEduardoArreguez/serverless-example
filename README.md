# serverless-cars-api

First approach to serverless and AWS Lambdas, featuring:
* DynamoDB
* SNS (Simple Notification Service)
* SES (Simple Email Service)