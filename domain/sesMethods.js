const {
  createEmailTemplate,
  deleteEmailTemplate,
  sendEMail,
} = require("../infrastructure/sesService");

async function sendTemplatedEmail(
  fromEmailAddress,
  toEmailAddresses = [],
  emailSubject,
  htmlMsgBody
) {
  const templateName = "template-" + new Date().getSeconds();
  try {
    await createEmailTemplate(templateName, emailSubject, htmlMsgBody);
    await sendEMail(templateName, fromEmailAddress, toEmailAddresses);
    await deleteEmailTemplate(templateName);
  } catch (err) {
    console.log(err.toString());
    await deleteEmailTemplate(templateName);
  }
}

module.exports = { sendTemplatedEmail };
