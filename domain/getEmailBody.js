"use strict";

const { getFromS3 } = require("../infrastructure/s3Service");

async function getEmailBody(fileName) {
  return getFromS3(process.env.EMAIL_TEMPLATE_BUCKET_NAME, fileName);
}

module.exports = { getEmailBody };
