"use strict";

const { getCar, putCar, postCar } = require("../infrastructure/bffService");

async function bffPostCar(carData, carId) {
  const getCarResponse = await getCar(carId);
  if (getCarResponse.body) {
    return putCar(carData, carId);
  } else {
    return postCar(carData);
  }
}

module.exports = { bffPostCar };
