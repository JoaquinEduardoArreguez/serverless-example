const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url= 'http://localhost:3000/stub';

describe('Save a car: ',()=>{

	it('should post a car', (done) => {
		const car = {
			brand: 'Peugeot',
			model: '308', 
			id: '123321'
		}

		chai.request(url)
			.post('/bff/cars')
			.send(car)
			.end((err, res) => {
				expect(res.body).to.deep.equal(car);
				expect(res).to.have.status(200);
				done();
			});
	});
});
