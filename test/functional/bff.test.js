const chai = require("chai");
const expect = chai.expect;
const bffPostCar = require("../../domain/bffPostCar");
const sinon = require("sinon");
const superagent = require("superagent");

describe("bffPostCar functional test", function () {
  let car;

  before(() => {
    car = { brand: "Peugeot", model: "308", id: "123321" };
  });

  beforeEach(() => {
    this.get = sinon.stub(superagent, "get").resolves({ body: car });
    this.put = sinon
      .stub(superagent, "put")
      .returns({ send: () => Promise.resolve(car) });
  });

  afterEach(() => {
    this.get.restore();
    this.put.restore();
  });

  it("Should SUCCED", async function () {
    let result;
    try {
      result = await bffPostCar.bffPostCar(car);
    } catch (error) {
      result = error.message;
    }
    console.log(result);
    expect(result).to.be.equal(car);
  });
});
