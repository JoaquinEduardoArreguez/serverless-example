const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const expect = chai.expect;

const {
  idSchema,
  carsSchema,
} = require("../../application/schemas/dbReqSchema");

const uuid = require("uuid-random");

describe("Car id validator", function () {
  it("Should FAIL with invalid ID", async function () {
    const id = "Marge";

    let result;
    try {
      result = await idSchema.validateAsync(id);
    } catch (error) {
      result = error.message;
    }

    expect(result).to.be.equal("id must be a valid number or uuid");
  });

  it("Should SUCCEED with valid id", async function () {
    const id = "1";
    const id2 = uuid();

    let result, result2;
    try {
      result = await idSchema.validateAsync(id);
    } catch (error) {
      result = error.message;
    }

    try {
      result2 = await idSchema.validateAsync(id2);
    } catch (error) {
      result2 = error.message;
    }

    expect(result).to.be.equal(1);
    expect(result2).to.be.equal(id2);
  });
});

describe("Car data validator", function () {
  it("Should FAIL with invalid brand", async function () {
    let params = {
      brand: "Lamborgu_ini",
      model: "spider",
      id: 1,
    };
    let result;

    try {
      result = await carsSchema.validateAsync(params);
    } catch (error) {
      result = error.message;
    }

    expect(result).to.be.equal(
      '"brand" with value "Lamborgu_ini" fails to match the required pattern: /^[a-zA-Z0-9 ]*$/'
    );
  });

  it("Should FAIL with invalid model", async function () {
    let params = {
      brand: "Lamborguini",
      model: "spider_",
      id: 1,
    };
    let result;

    try {
      result = await carsSchema.validateAsync(params);
    } catch (error) {
      result = error.message;
    }

    expect(result).to.be.equal(
      '"model" with value "spider_" fails to match the required pattern: /^[a-zA-Z0-9 ]*$/'
    );
  });

  it("Should SUCCEED with valid brand and model", async function () {
    let params = {
      brand: "toyota",
      model: "hilux",
    };
    let result;

    try {
      result = await carsSchema.validateAsync(params);
    } catch (error) {
      result = error.message;
    }

    expect(result).to.be.an("object");

    expect(result).to.eql(params);
  });
});
